package apitools

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

var (
	// MaxPayloadSize is 1mb
	MaxPayloadSize = int(1048576)
	// ErrTooLarge is returned when req body size is > MaxPayloadSize
	ErrTooLarge = errors.New("Payload too large")
)

// UUIDInput ...
type UUIDInput struct {
	ID string `json:"id"`
}

// IDInput ...
type IDInput struct {
	ID int `json:"id"`
}

// ItemsBody is a response format when asking for a slice type datas
type ItemsBody struct {
	Data  interface{} `json:"data"`
	Items int         `json:"items"`
}

// ErrorBody is an error from api
type ErrorBody struct {
	ErrorMsg *string `json:"error,omitempty"`
}

// OPENFAASJsonResponse send a json response
func OPENFAASJsonResponse(w http.ResponseWriter, status int, body interface{}) {
	var err error
	var bodyByte []byte
	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("Access-Control-Allow-Methods", "GET,POST,DELETE,PATCH,OPTIONS")
	w.Header().Add("Access-Control-Allow-Headers", "Content-Type,Date,Authorization,X-Xss-Protection,Content-Length")
	if bodyByte, err = json.Marshal(body); err != nil {
		OPENFAASErrorResponse(w, http.StatusInternalServerError, err)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(bodyByte)
	return
}

// OPENFAASErrorResponse write an error response in openfaas context
func OPENFAASErrorResponse(w http.ResponseWriter, status int, err error) {
	log.SetPrefix("ERROR ")
	log.Printf("%s\n", err.Error())
	w.WriteHeader(status)
	nmsg := map[string]string{"error": err.Error()}
	resp, err := json.Marshal(nmsg)
	if err != nil {
		log.Printf("%s\n", err.Error())
	}
	_, err = w.Write(resp)
	if err != nil {
		log.Printf("%s\n", err.Error())
	}
	return
}

// OPENFAASGetBody check and deserialze the req payload in handler
func OPENFAASGetBody(r *http.Request, handler interface{}) error {
	var err error
	var body []byte
	if r == nil || r.Body == nil {
		return errors.New("invalid or null request body")
	}
	defer r.Body.Close()
	if body, err = ioutil.ReadAll(r.Body); err != nil {
		return nil
	}
	if len(body) > MaxPayloadSize {
		return ErrTooLarge
	}
	dec := json.NewDecoder(bytes.NewReader(body))
	dec.DisallowUnknownFields()
	if err := dec.Decode(&handler); err != nil {
		fmt.Printf("Error(OPENFAASGetBody): %s\n", err.Error())
		return err
	}
	return nil
}
