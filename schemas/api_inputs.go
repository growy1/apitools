package schemas

import (
	"time"

	"gitlab.com/growy1/seed/schemas"
)

// AlarmInput ...
type AlarmInput struct {
	On   schemas.DataType `json:"on"`
	Low  *MeasureInput    `json:"low"`
	High *MeasureInput    `json:"high"`
}

// APIKey ...
type APIKey struct {
	ID       string     `json:"id"`
	User     string     `json:"user"`
	Key      string     `json:"key"`
	ExpireAt *time.Time `json:"expireAt"`
}

// CannabinoidInput ...
type CannabinoidInput struct {
	Name       string  `json:"name"`
	Percentage float64 `json:"percentage"`
}

// DeviceInput ...
type DeviceInput struct {
	Name string             `json:"name"`
	Type schemas.DeviceType `json:"type"`
}

// DeviceStockInput ...
type DeviceStockInput struct {
	DeviceID int     `json:"deviceId"`
	Count    float64 `json:"count"`
	Unit     string  `json:"unit"`
}

// GrowStateInput ...
type GrowStateInput struct {
	DevelopmentState schemas.DevelopmentStateType `json:"developmentState"`
	StartedAt        time.Time                    `json:"startedAt"`
	EndedAt          *time.Time                   `json:"endedAt"`
	Comments         *string                      `json:"comments"`
}

// GrowingSessionInput ...
type GrowingSessionInput struct {
	Name      string                      `json:"name"`
	Type      schemas.GrowingSessionType  `json:"type"`
	State     schemas.GrowingSessionState `json:"state"`
	StartedAt time.Time                   `json:"startedAt"`
	EndedAt   *time.Time                  `json:"endedAt"`
	BoxVolume []MeasureInput              `json:"boxVolume"`
	Plants    []PlantInput                `json:"plants"`
	UserID    string                      `json:"userId"`
}

// GrowingSessionSearchInput ...
type GrowingSessionSearchInput struct {
	State *schemas.GrowingSessionState `json:"State"`
	Type  *schemas.GrowingSessionType  `json:"Type"`
}

// HarvestStockInput ...
type HarvestStockInput struct {
	PlantID string  `json:"plantId"`
	Count   float64 `json:"count"`
	Unit    string  `json:"unit"`
}

// MeasureInput ...
type MeasureInput struct {
	Date  *time.Time       `json:"date"`
	Type  schemas.DataType `json:"type"`
	Value float64          `json:"value"`
	Unit  string           `json:"unit"`
}

// PaginationInput ...
type PaginationInput struct {
	Count  int `json:"Count"`
	Offset int `json:"Offset"`
}

// PlantInput ...
type PlantInput struct {
	Name      string     `json:"name"`
	StartedAt time.Time  `json:"startedAt"`
	EndedAt   *time.Time `json:"endedAt"`
	StrainID  *int       `json:"strainId"`
}

// PlantSearchInput ...
type PlantSearchInput struct {
	Name         *string    `json:"Name"`
	StartedSince *time.Time `json:"startedSince"`
	EndedBefore  *time.Time `json:"endedBefore"`
	StrainID     *int       `json:"strainId"`
}

// RegisterInput ...
type RegisterInput struct {
	Name          string  `json:"name"`
	Email         *string `json:"email"`
	Password      string  `json:"password"`
	InvitationKey string  `json:"invitationKey"`
}

// SeedBankInput ...
type SeedBankInput struct {
	Name    string `json:"name"`
	LogoURL string `json:"logoUrl"`
}

// SeedFinderInput ...
type SeedFinderInput struct {
	SeedFinderID        string `json:"seedFinderId"`
	SeedFinderBreederID string `json:"seedFinderBreederId"`
}

// SeedStockInput ...
type SeedStockInput struct {
	StrainID int     `json:"strainId"`
	Count    float64 `json:"count"`
	Unit     string  `json:"unit"`
}

// SeedStockUpdateInput ...
type SeedStockUpdateInput struct {
	ID       string   `json:"id"`
	StrainID *int     `json:"strainId"`
	Count    *float64 `json:"count"`
	Unit     *string  `json:"unit"`
}

// Session ...
type Session struct {
	User  *schemas.User `json:"user"`
	Otp   bool          `json:"otp"`
	Token string        `json:"token"`
}

// StrainInput ...
type StrainInput struct {
	Supplier       string                    `json:"supplier"`
	Link           string                    `json:"link"`
	SeedBankID     *int                      `json:"seedBankId"`
	Name           string                    `json:"name"`
	Genetic        string                    `json:"genetic"`
	Photoperiod    schemas.StrainPhotoperiod `json:"photoperiod"`
	Type           []StrainTypeInput         `json:"type"`
	Cannabinoids   []CannabinoidInput        `json:"cannabinoids"`
	FloweringTimes []MeasureInput            `json:"floweringTimes"`
}

// StrainTypeInput ...
type StrainTypeInput struct {
	Type       schemas.StrainType `json:"type"`
	Percentage *float64           `json:"percentage"`
}

// UpdateGrowStateInput ...
type UpdateGrowStateInput struct {
	ID               string                        `json:"id"`
	DevelopmentState *schemas.DevelopmentStateType `json:"developmentState"`
	StartedAt        *time.Time                    `json:"startedAt"`
	EndedAt          *time.Time                    `json:"endedAt"`
	Comments         *string                       `json:"comments"`
}

// UpdatePictureInput ...
type UpdatePictureInput struct {
	ID       string     `json:"id"`
	FilePath *string    `json:"filePath"`
	TakenAt  *time.Time `json:"takenAt"`
}

// UpdatePlantInput ...
type UpdatePlantInput struct {
	ID        string     `json:"id"`
	Name      *string    `json:"name"`
	StartedAt *time.Time `json:"startedAt"`
	EndedAt   *time.Time `json:"endedAt"`
	StrainID  *int       `json:"strainId"`
}

// UpdateStrainInput ...
type UpdateStrainInput struct {
	ID          int                        `json:"id"`
	Link        *string                    `json:"link"`
	SeedBankID  *int                       `json:"seedBankId"`
	Name        *string                    `json:"name"`
	Genetic     *string                    `json:"genetic"`
	Photoperiod *schemas.StrainPhotoperiod `json:"photoperiod"`
}

// UpdateUserInput ...
type UpdateUserInput struct {
	ID        string  `json:"id"`
	Name      *string `json:"name"`
	Email     *string `json:"email"`
	Password  *string `json:"password"`
	Inventory *bool   `json:"inventory"`
}
