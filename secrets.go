package apitools

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
)

// Stage is the stage
var Stage = func() string {
	envar := os.Getenv("STAGE")
	if envar == "" {
		return "DEV"
	}
	return envar
}()

// Context is the running context
var Context = os.Getenv("CONTEXT")

// Verbose is log verbosity level
var Verbose = func() bool {
	return os.Getenv("VERBOSE") != ""
}()

func getKubeSecret(secretName string) (secretVal string, err error) {
	var secretDir = os.Getenv("SECRETS_PATH")
	var secretBytes []byte
	if strings.Contains(secretName, "_") {
		secretName = strings.ReplaceAll(secretName, "_", "-")
	}
	secretName = strings.ToLower(secretName)
	if secretBytes, err = ioutil.ReadFile(secretDir + secretName); err != nil {
		return "", err
	}
	secretString := strings.TrimSuffix(string(secretBytes), "\n")
	return secretString, err
}

// MustGetSecret from secret storage or env var (panic if failed)
func MustGetSecret(secretName string) (secret string) {
	var err error
	defVal := os.Getenv(secretName)
	if Context == "KUBE" {
		if defVal, err = getKubeSecret(secretName); err != nil {
			log.Fatal(err)
		}
	}
	if defVal == "" {
		log.Fatalf("missing secret %s", secretName)
	}
	return defVal
}
